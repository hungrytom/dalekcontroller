#include "Observation.h"

using namespace Dalek;
using namespace cv;
using namespace std;

Observation::~Observation()
{
    //dtor
}

Observation::Observation(Mat& frame) {
	this->img = frame;
}

inline Mat Dalek::Observation::getImage() {
	return this->img;
}

const char* Dalek::Observation::getImageBytes() {
	string s;
	stringstream ss;
	ss << format(this->img,"csv");
	ss >> s;
	return s.c_str();
}
