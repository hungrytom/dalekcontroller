################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DalekCommunicator.cpp \
../src/DalekController.cpp \
../src/DalekDriver.cpp \
../src/DalekObservation.cpp \
../src/DalekVisionController.cpp 

OBJS += \
./src/DalekCommunicator.o \
./src/DalekController.o \
./src/DalekDriver.o \
./src/DalekObservation.o \
./src/DalekVisionController.o 

CPP_DEPS += \
./src/DalekCommunicator.d \
./src/DalekController.d \
./src/DalekDriver.d \
./src/DalekObservation.d \
./src/DalekVisionController.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


