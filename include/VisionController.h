#ifndef VISIONCONTROLLER_H
#define VISIONCONTROLLER_H

#include "Observation.h"
#include <iostream>
#include <memory>
#include <opencv2/opencv.hpp>
#include <exception>

using namespace std;
using namespace cv;

namespace Dalek {

	class CameraErrorException: public exception {
		const char* msg;

		public:
			CameraErrorException(const char* m) {
				this->msg = m;
			}
			virtual const char* what() const throw() {
				return msg;
			}
	};

	/**
		# Dalek Vision Controller #

		Handles camera interactions and provides methods for accessing
		camera functionality and vision features
	*/
	class VisionController
	{
		public:
			VisionController();
			virtual ~VisionController();

			/** Makes a new observation and returns it */
			Observation observe() throw (CameraErrorException);

			/** Start camera loop capturing images and pushing to stream
			 * 	in a separate thread. */
			void startObserving() throw (CameraErrorException);

			/** Stop the camera loop */
			void stopObserving();

		protected:

		private:
			VideoCapture cam;
	};
}
#endif // VISIONCONTROLLER_H
