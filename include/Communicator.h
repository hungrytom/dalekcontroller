#ifndef COMMUNICATOR_H_INCLUDED
#define COMMUNICATOR_H_INCLUDED

#include "MessageProcessor.h"
#include <string>
#include <memory>

using namespace std;

namespace Dalek {

	/**
	 *  # Dalek Communicator #
	 *
	 *  An abstract class that provides methods for communicating with
	 *  clients over the web.
	 *
	 *  Includes functionality to allow clients to subscribe to a video
	 *  feed and handles messages from clients using given callbacks.
	 *
	 *  There will eventually be at least 2 clients of this component:
	 *  the user - who will control the dalek and receive AV streams
	 *  			(and eventually transmit AV streams to the Dalek)
	 *  and the SLAM master - who will store observations and process
	 *  						them to maintain a map and localisation
	 *  						estimate
	 *
	 *  There is also the possibility of multiple "passenger" clients
	 *  who will receive AV streams but will not be able to send
	 *  instructions to the Dalek (but possibly send AV streams).
	 *
	 *  Notice the pure virtual functions (e.g. virtual void addCallback(..) = 0;)
	 *  which require subclasses to implement this interface.
	 */
	class Communicator {
		public:
			Communicator();
			virtual ~Communicator();

			/** Starts the server loop, waiting for a client to connect and listening for messages. */
			virtual void startListening() = 0;

			/** Stops the thread listening for messages */
			virtual void stopListening() = 0;

			/** Add a callback function to process messages */
			virtual void addCallback(shared_ptr<MessageProcessor> messageProcessor) = 0;

			/** Returns true if listening loop is active, false otherwise */
			virtual bool isListening() = 0;
	};

}
#endif // COMMUNICATOR_H_INCLUDED
