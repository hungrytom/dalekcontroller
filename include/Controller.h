#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include "Communicator.h"
#include "VisionController.h"
#include "Driver.h"
#include <iostream>
#include <memory>
#include <string>

using namespace std;

/**
 * All objects belonging to the Dalek system will be provided within
 * the "Dalek" namespace
 */
namespace Dalek {

	/**
		# Controller #

		Provides a server process which listens for & processes instructions
				including:

				1. transmitting video to the client
				2. passing movement commands to the driver

		Note that the Controller has 2 modes:
		SUBSERVIENT - obeys (depends on) instructions from a connected
						user "driver" client to dictate movement
		EVIL_AND_AUTONOMOUS - ignores instructions related to platform
								movement from "driver" client and instead
								obeys instructions from "SLAM master".
								"driver" clients must submit requests to
								the SLAM master which will schedule a
								route plan for the Dalek to follow.
	*/
	class Controller {
		public:
			/** Mode of operation for a Dalek */
			enum class Mode : unsigned short { SUBSERVIENT = 0, EVIL_AND_AUTONOMOUS};

			/** Default constructor */
			Controller(Mode startMode,
						shared_ptr<Communicator> comm,
						shared_ptr<Driver> driver,
						shared_ptr<VisionController> vision);
			/** Destructor */
			virtual ~Controller();

			/** Set the operating mode of the system */
			void setMode(Mode m);
			/** Get the operating mode of the system */
			Mode getMode();

			/** Output the state of the system to the given stream */
			void printState(ostream &os);

		protected:
			/** The current operating mode of the system */
			Mode mode;

		private:
			/** Provides functions for communicating with the client */
			shared_ptr<Communicator> comm;
			/** Provides functions for interacting with the webcam */
			shared_ptr<VisionController> vision;
			/** Provides functions for movement of the Dalek mobile platform */
			shared_ptr<Driver> driver;
	};

}
#endif // CONTROLLER_H_INCLUDED
