/*
 * SocketCommunicator.cpp
 *
 *  Created on: 13 Oct 2013
 *      Author: tom
 */

#include "SocketCommunicator.h"

using namespace Dalek;
using namespace std;

/* default listen port number */
#define DEF_LISTEN_PORT		8081

/* default socket backlog number. SOMAXCONN is a system default value */
#define DEF_SOCKET_BACKLOG	SOMAXCONN

/* default buffer size */
#define BUFSIZE			4096

/* useful macro */
#define CRLF_STR		"\r\n"

SocketCommunicator::SocketCommunicator() {
	this->is_listening = shared_ptr<atomic_bool>(new atomic_bool(false));
}

/**
 * Stops the listener thread if running...
 */
SocketCommunicator::~SocketCommunicator() {
	this->stopListening();
}

/**
 * Processes messages and sends response
 */
static int do_serv_task(apr_socket_t *sock, apr_pool_t *mp, list<shared_ptr<MessageProcessor>> &callbacks) {
	char buf[BUFSIZE];
	apr_size_t len = sizeof(buf) - 1;/* -1 for a null-terminated */

	while(1) {
		apr_status_t rv = apr_socket_recv(sock, buf, &len);
		if (rv == APR_EOF || len == 0) {
			break;
		}
		if (strstr(buf, CRLF_STR CRLF_STR)) {/* expect the end of the request. no guarantee */
			break;
		}
		const char *msg = buf;
		for(auto &i : callbacks) {
			cout << "listenForMessages..." << msg << endl;
			MessageProcessor *mp = i.get();
			if(mp) {
				const char * response = mp->processMessage(msg);
				apr_size_t len = strlen(response);
				apr_socket_send(sock, response , &len);
			}
		}
	}

	return 0;
}

/**
 * Create a listening socket, and listen it.
 */
static apr_status_t do_listen(apr_socket_t **sock, apr_pool_t *mp) {
	apr_status_t rv;
	apr_socket_t *s;
	apr_sockaddr_t *sa;

	rv = apr_sockaddr_info_get(&sa, NULL, APR_INET, DEF_LISTEN_PORT, 0, mp);
	if (rv != APR_SUCCESS) {
		return rv;
	}

	rv = apr_socket_create(&s, sa->family, SOCK_STREAM, APR_PROTO_TCP, mp);
	if (rv != APR_SUCCESS) {
		return rv;
	}

	/* it is a good idea to specify socket options explicitly.
	 * in this case, we make a blocking socket as the listening socket */
	apr_socket_opt_set(s, APR_SO_NONBLOCK, 0);
	apr_socket_timeout_set(s, -1);
	apr_socket_opt_set(s, APR_SO_REUSEADDR, 1);/* this is useful for a server(socket listening) process */

	rv = apr_socket_bind(s, sa);
	if (rv != APR_SUCCESS) {
		return rv;
	}
	rv = apr_socket_listen(s, DEF_SOCKET_BACKLOG);
	if (rv != APR_SUCCESS) {
		return rv;
	}

	*sock = s;
	return rv;
}

void listenForMessages(list<shared_ptr<MessageProcessor>> &callbacks,
		shared_ptr<atomic_bool> is_listening) throw (SocketException) {
	apr_status_t rv;
	apr_pool_t *mp;
	apr_socket_t *s;/* listening socket */

	apr_initialize();
	apr_pool_create(&mp, NULL);

	rv = do_listen(&s, mp);
	if (rv != APR_SUCCESS) {
		throw SocketException();
	}

	while (is_listening.get()->load()) {
		apr_socket_t *ns;/* accepted socket */

		rv = apr_socket_accept(&ns, s, mp);
		if (rv != APR_SUCCESS) {
			throw SocketException();
		}
		/* it is a good idea to specify socket options for the newly accepted socket explicitly */
		apr_socket_opt_set(ns, APR_SO_NONBLOCK, 0);
		apr_socket_timeout_set(ns, -1);

		if (!do_serv_task(ns, mp, callbacks)) {
			throw SocketException();
		}
		apr_socket_close(ns);
	}

	apr_pool_destroy(mp);
	apr_terminate();
}

void SocketCommunicator::startListening() throw (SocketException) {
	if (!(is_listening.get()->load())) {
		this->is_listening.get()->store(true);
		this->listener_thread = thread(listenForMessages, callbacks,
				is_listening);
	} else {
		cerr << "Already listening";
	}
}

void SocketCommunicator::stopListening() {
	if ((is_listening.get()->load())) {
		this->is_listening.get()->store(false);
		this->listener_thread.join();
	} else {
		cerr << "Already stopped listening";
	}
}

void SocketCommunicator::addCallback(
		shared_ptr<MessageProcessor> messageProcessor) {
	callbacks.push_back(messageProcessor);
}

bool SocketCommunicator::isListening() {
	return this->is_listening.get()->load();
}
