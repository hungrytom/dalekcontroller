#ifndef DRIVER_H
#define DRIVER_H

namespace Dalek {

	/**
		# Dalek Driver #

		Provides functions for moving around and
		causing trouble

		Note that the Dalek has a catepillar track
		platform (left and right track) each capable of
		moving forwards or backwards in order to
		achieve 2D motion across a flat surface.

		There is a second system for moving the head
		unit separately from on top of the primary
		platform. This is a single motor which can
		turn the head right or left up to 90 degrees
		in both directions (0 degrees is facing forwards).

		There is also a servo that controls the angle of
		the camera.
	*/
	class Driver
	{
		public:
			Driver();
			virtual ~Driver();

			/** Sets the speed of the robot primary platform movement */
			void setSpeed(double speed);

			/** Moves the robot platform forwards across the
				flat surface until told to stop. */
			void moveForwards();

			/** Moves the robot platform backwards across the
				flat surface until told to stop. */
			void moveBackwards();

			/** Stops the robot platform from moving */
			void stop();

			/** Turns the robot platform right until told to stop. */
			void turnRight();

			/** Turns the robot platform left until told to stop. */
			void turnLeft();

			/** Turns the robot head unit right at the given speed
				(negative speed turns left) until told to stopHead(). */
			void turnHead(double speedRight);

			/** Stop moving robot head */
			void stopHead();

			/** Raise(/lower) the robot camera given amount */
			void raiseCamera(double amount);
		protected:
			/** Available actuators on Dalek platform */
			enum Actuator {LEFT_TRACK, RIGHT_TRACK, HEAD_MOTOR, CAMERA_SERVO};
			/** Set the given actuator value to effect/stop movement */
			void setActuator(Actuator a, double value);
		private:
			/** Primary platform speed of both cat tracks */
			double speed;
	};

}

#endif // DRIVER_H
