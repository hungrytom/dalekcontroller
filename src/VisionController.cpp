#include "VisionController.h"

using namespace Dalek;
using namespace cv;

VisionController::VisionController()
{
	this->cam = 0;
}

VisionController::~VisionController()
{
    //dtor

	// Send EOF down any output stream current being handled
	// such as the video output stream (returned by getVisionStream())
	// This will instruct all consumers to close the stream
}

Observation Dalek::VisionController::observe() throw (CameraErrorException) {
    if(!cam.isOpened())
        throw CameraErrorException("Failed to observe");
    Mat frame;
    cam >> frame;
    Observation obs(frame);
    return obs;
}

void Dalek::VisionController::startObserving() throw (CameraErrorException) {
	cam = VideoCapture(0);
    if(!cam.isOpened())
        throw CameraErrorException("Failed to initialise camera");
}

void Dalek::VisionController::stopObserving() {
	cam.release();
}
