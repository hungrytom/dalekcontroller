/*
 * MessageProcessor.h
 *
 *  Created on: 13 Oct 2013
 *      Author: tom
 */

#ifndef MESSAGEPROCESSOR_H_
#define MESSAGEPROCESSOR_H_

#include "Driver.h"
#include "VisionController.h"
#include <iostream>
#include <memory>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

using namespace std;

namespace Dalek {

	/**
	 * # Message Processor #
	 *
	 * Provides a callback function for processing messages
	 * and contains dependency objects for use in message processing.
	 *
	 * For example, the processMessage function could pass the message
	 * string through a set of regex logic matching to determine which
	 * operation to perform on the Dalek.
	 *
	 * This is essentially the implementation of the protocol for
	 * communications between the clients and server.
	 */
	class MessageProcessor {
		public:
			MessageProcessor(shared_ptr<Driver> driver);
			virtual ~MessageProcessor();

			/** Process the given message and return a response
			 * */
			virtual const char * processMessage(const char* msg);

			/**
			 * Add a reference to a vision controller to provide clients with
			 * observations or a continuous feed
			 */
			virtual void registerVisionProvider(shared_ptr<VisionController> visionController);

		private:
			shared_ptr<VisionController> vision_controller;
			shared_ptr<Driver> driver;
	};

}

#endif /* MESSAGEPROCESSOR_H_ */
