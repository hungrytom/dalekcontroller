#ifndef OBSERVATION_H
#define OBSERVATION_H

#include <opencv2/opencv.hpp>

using namespace cv;

namespace Dalek {
	/**
		# Dalek Observation #

		Contains an observation from the webcam
		along with features data for use in mapping
	*/
	class Observation
	{
		public:
			Observation(Mat &frame);
			virtual ~Observation();

			// Feature[] getFeatures();
			/** Provides a copy of the image header and reference
			 * counted pointer to the image matrix in a OpenCV Mat
			 * object. */
			Mat getImage();

			/** Returns the image as a string of bytes */
			const char *getImageBytes();
		protected:
		private:
			Mat img;
	};
}

#endif // OBSERVATION_H
