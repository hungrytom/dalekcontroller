/*
 * MessageProcessor.cpp
 *
 *  Created on: 13 Oct 2013
 *      Author: tom
 */

#include <MessageProcessor.h>

using namespace std;
using namespace Dalek;

MessageProcessor::MessageProcessor(shared_ptr<Driver> driver) {
	this->driver = driver;
}

MessageProcessor::~MessageProcessor() {
}

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

double string_to_double(const string& s) {
	std::istringstream i(s);
	double x;
	if (!(i >> x))
		return 0;
	return x;
}

enum Instruction {
	Observe, Move, Turn, TurnHead, RaiseCamera, Stop
};

const char * MessageProcessor::processMessage(const char *msg) {
	cout << "processMessage(" << msg << ")" << endl; // TODO remove
	string m(msg);
	m = trim(m);

	Instruction ins;
	if(m.compare("observe")  == 0)
		ins = Observe;
	else if(m.find("move") != string::npos)
		ins = Move;
	else if(m.find("turn") != string::npos)
		ins = Turn;
	else if(m.find("turnhead") != string::npos)
		ins = TurnHead;
	else if(m.find("raisecam") != string::npos)
		ins = RaiseCamera;
	else if(m.find("stop") != string::npos)
		ins = Stop;

	switch(ins) {
		case Observe:
			if(this->vision_controller && this->vision_controller.get()) {
				Observation obs = this->vision_controller.get()->observe();
				return obs.getImageBytes();
			} else {
				return "Error - no vision controller available.";
			}
			break;
		case Move:
		case Turn:
		case TurnHead:
		case RaiseCamera:
		case Stop:
			double speed = string_to_double(m.substr(m.find_first_of(" ")));
			if(this->driver && this->driver.get()) {
				Driver *d = this->driver.get();

//				d->setSpeed(speed);
//
//				switch(ins) {
//				case Observe:
//					break;
//				case Move:
//					if(speed>0)
//						d->moveForwards();
//					else if(speed<0)
//						d->moveBackwards();
//					else
//						d->stop();
//					break;
//				case Turn:
//					if(speed>0)
//						d->turnRight();
//					else if(speed<0)
//						d->turnLeft();
//					else
//						d->stop();
//					break;
//				case TurnHead:
//					d->turnHead(speed);
//					break;
//				case RaiseCamera:
//					d->raiseCamera(speed);
//					break;
//				case Stop:
//					d->stop();
//					break;
//				}
			} else {
				return "Error - no driver available.";
			}
			break;
	}
	return "msg";
}

void MessageProcessor::registerVisionProvider(shared_ptr<VisionController> visionController) {
	this->vision_controller = visionController;
}

