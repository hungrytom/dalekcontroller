#include "Controller.h"
#include "Driver.h"
#include "VisionController.h"
#include "SocketCommunicator.h"
#include <memory>

using namespace std;
using namespace Dalek;

/**
 * The entry point for the Controller application
 */
int main(int argz, char** argv) {
	cout << "# Dalek Controller - by Tom Wilcox #" << endl;

	shared_ptr<Driver> driver;
	shared_ptr<VisionController> vision;

	shared_ptr<MessageProcessor> messageProcessor(new MessageProcessor(driver));
	shared_ptr<Communicator> comm(new SocketCommunicator());

	comm.get()->addCallback(messageProcessor);
	messageProcessor.get()->registerVisionProvider(vision);

	unique_ptr<Controller> dc(new Controller(Controller::Mode::SUBSERVIENT,
												comm, driver, vision));

	dc.get()->printState(cout);
	while(comm->isListening()) {
		this_thread::sleep_for(chrono::seconds(1));
	}

	cout << "# Dalek Controller Terminating #" << endl;

	return 0;
}
