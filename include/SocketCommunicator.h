#ifndef SOCKETCOMMUNICATOR_H_
#define SOCKETCOMMUNICATOR_H_

#include "Communicator.h"
#include "MessageProcessor.h"
#include <exception>
#include <iostream>
#include <thread>
#include <list>
#include <algorithm>
#include <atomic>
#include <iterator>

#include <apr_general.h>
#include <apr_file_io.h>
#include <apr_strings.h>
#include <apr_network_io.h>

namespace Dalek {
	class SocketException : public exception {};

	/**
	 * Provides an implementation of the communicator
	 * component using sockets with limited functionality
	 *
	 * TODO Implement a SIPCommunicator to replace this
	 *
	 * Note that here it would be sensible to append the "override"
	 * keyword to the end of the virtual function implementations
	 * to indicate overridden functions to the compiler. However,
	 * this compiler is out-of-date.
	 *
	 * Uses Apache Portable Runtime to implement a socket-based server
	 */
	class SocketCommunicator : public Communicator {
	public:
		SocketCommunicator();
		virtual ~SocketCommunicator();

		/** Starts the server loop, waiting for a client to connect and listening for messages. */
		virtual void startListening() throw (SocketException);

		/** Stop listening and destroy listener thread */
		virtual void stopListening();

		/** Add a callback function to process messages */
		virtual void addCallback(shared_ptr<MessageProcessor> messageProcessor);

		virtual bool isListening();

	private:
		list<shared_ptr<MessageProcessor>> callbacks;
		thread listener_thread;
		shared_ptr<atomic_bool> is_listening;
	};

}

#endif /* SOCKETCOMMUNICATOR_H_ */
