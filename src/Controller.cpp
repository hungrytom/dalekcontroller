#include "Controller.h"
#include "SocketCommunicator.h"
#include <sstream>

using namespace Dalek;

Controller::Controller(Mode startMode, shared_ptr<Communicator> comm, shared_ptr<Driver> driver, shared_ptr<VisionController> vision) {
	this->mode = startMode;
	this->comm = comm;
	this->driver = driver;
	this->vision = vision;
	try {
		this->comm.get()->startListening();
	} catch(SocketException& ex) {
	    cout << ex.what() << endl;
	}
}

Controller::~Controller() {
}

inline void Controller::setMode(Mode m) {
	this->mode = m;
}

inline Controller::Mode Controller::getMode() {
	return this->mode;
}

void Controller::printState(ostream& os) {
	os << "Controller:" << endl;
	os << "mode:" << static_cast<unsigned int>(this->mode) << endl;
}
